/**
 * The MIT License
 * Copyright (c) 2017-2018 Erik Partridge
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <cstring>
#include <iostream>
#include <utility> #include "client.h"

std::string client::build_message(std::string action, char* contents, std::string type, bool async)
{
    std::string result = "";
    result += action + "\n";
    if(async) {
        result += "async\n";
    } else{
        result += "\n";
    }
    result += "content-type: " + type + "\n";
    // Do this here so we know how many bytes we'll need to read, include null terminator by adding 1
    size_t content_length = strlen(contents) + 1;
    result += "content-length: " + std::to_string(content_length) + "\n";
    result += std::string(contents);
    return result;
}

std::string client::send_message(std::string action, char* contents, std::string type, bool async)
{
    char buffer[1024];
    HANDLE pipe;
    DWORD amount;
    //Connect to the already created pipe
    pipe = CreateFile(TEXT(PIPE_NAME), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

    std::string message = build_message(std::move(action), contents, type, async);
    unsigned long length = message.size() + 1; // Null terminate
    WriteFile(pipe, message, length, &amount, NULL);
    std::string res = "";
    //Buffer the input
    while (ReadFile(pipe, buffer, 1024, &amount, NULL) != false && amount != 0){
        //This will automatically construct a string of the length read in.
        res += std::string(buffer, amount);
    }
    CloseHandle(pipe);
    return res;
}
