/**
 * The MIT License
 * Copyright (c) 2017-2018 Erik Partridge
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef STREAMLABS_CLIENT_H
#define STREAMLABS_CLIENT_H

#include <string>
#define PIPE_NAME "\\\\.\\pipe\\j24"


class client {

public:

    /**
     * Generates the message to be passed through the pipe
     * @param action the action to be performed
     * @param contents the contents of the message. For GET, this will be the ID. For STORE, the contents. For GET_ATTR, ID:Attribute
     * @param async if the message will eventually be passed asynchronously
     * @param type the type of the data being passed
     * @return the message to pass to the server
     */
    std::string build_message(std::string action, char* contents, std::string type, bool async = false);

    /**
     * Build and send a message to the server, and return the result
     * @param action  the command to be performed
     * @param contents  the contents of teh request
     * @param type the type of the parameter
     * @param async if it should be performed asynchronously on the server
     * @return the response from the server
     */
    std::string send_message(std::string action, char* contents, std::string type, bool async = false);

};

#endif //STREAMLABS_CLIENT_H
