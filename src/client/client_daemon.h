/**
 * The MIT License
 * Copyright (c) 2017-2018 Erik Partridge
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef STREAMLABS_CLIENT_DAEMON_H
#define STREAMLABS_CLIENT_DAEMON_H

#include <iostream>
#include "client.h"

int main(int argc, char** argv){

    if (argc < 4) {
        //Give some basic documentation if the command is not properly used
        std::cerr << "Usage: " << argv[0] << " COMMAND TYPE ASYNC [CONTENTS]" << std::endl;
        std::cerr << "Command options: GET, STORE, DECREMENT, INCREMENT, APPEND, LENGTH" << std::endl;
        std::cerr << "Type options: STRING, INTEGER, FLOAT" << std::endl;
        std::cerr << "Example: client.exe GET INTEGER 0 INTEGER-1-1" << std::endl;
        std::cerr << "Example: client.exe STORE INTEGER 1 1230501329" << std::endl;
        return 1;
    } else {
        std::string command = argv[1];
        std::string type = argv[2];
        bool async = (bool) atoi(argv[3]);
        std::string contents = argv[4];
        client* c = new client();
        std::cout << c->send_message(command, const_cast<char*>(contents.c_str()), type, async) << std::endl;
        delete c;
        return 0;
    }
}

#endif //STREAMLABS_CLIENT_DAEMON_H
