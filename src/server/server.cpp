/**
 * The MIT License
 * Copyright (c) 2017-2018 Erik Partridge
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <future>
#include "server.h"
#include <sstream>
#include <utility>

std::string server::create_identifier(std::string type)
{
    time_t seconds = time(nullptr);
    std::string identifier = type +  "-" + std::to_string(seconds) + "-" + std::to_string(rand());
    return identifier;
}

std::string server::handle_store(std::string type, std::string bytes, bool async)
{

    std::string identifier = create_identifier(type);
    std::future<bool> fut = std::async(std::launch::async, &server::store_value, this, type, bytes, identifier);
    if(!async){
        bool success = fut.get();
    }
    return identifier;
}

bool server::store_value(const std::string& type, std::string bytes, const std::string& identifier)
{
    if(type == "INTEGER"){
        long long value = atoll(bytes.c_str());
        integers[identifier] = value;
    } else if (type == "FLOAT") {
        long double value = atof(bytes.c_str());
        floats[identifier] = value;
    } else if (type == "STRING"){
        std::string value = std::string(bytes);
        strings[identifier] = value;
    }
    return true;
}

std::string server::handle_get(std::string id)
{
    std::string type = id.substr(0, id.find('-'));
    if(type == "INTEGER") {
        long long value = this->integers[id];
        return std::to_string(value);
    } else if (type == "FLOAT") {
        long double value = this->floats[id];
        return std::to_string(value);
    } else if (type == "STRING") {
        return this->strings[id];
    } else {
        return "No such value";
    }
}

std::string server::parse_message(std::string message)
{
    std::stringstream stream(message);
    std::string line;
    std::string method;
    bool async = false;
    std::string type;
    int index = 0;
    std::string contents = "";
    while(std::getline(stream, line, '\n')) {
        switch(index){
        case 0:
            method = line;break;
        case 1:
            async = !line.empty();
            break;
        case 2:
            type = line.substr(14);
            break;
        case 3:
            break;
        default:
            contents += line;
        }
        index ++;
    }
    return handle_message(method, async, contents, type);
}
std::string server::handle_message(std::string method, bool async, std::string contents, std::string type)
{
    if(method == "STORE"){
        std::string identifier = this->handle_store(std::move(type), contents, async);
        return identifier;
    } else if (method == "GET"){
        return this->handle_get(contents);
    } else if (method == "INCREMENT" && (type == "INTEGER" || type == "FLOAT")) {
        this->handle_increment(contents);
        return contents;
    } else if (method == "DECREMENT" && (type == "INTEGER" || type == "FLOAT")) {
        this->handle_decrement(contents);
        return contents;
    } else if (method == "APPEND" && type == "STRING"){
        this->handle_append(contents);
        return this->strings[contents];
    } else if (method == "LENGTH" && type == "STRING"){
        return this->handle_length(contents);
    } else {
        return "Error: method not supported";
    }
}
bool server::has_contents()
{
    return !integers.empty();
}

void server::handle_increment(const std::string& contents)
{
    if(contents[0] == 'I')
        this->integers[contents] += 1;
    else
        this->floats[contents] += 1;
}

void server::handle_decrement(std::string id)
{
    if(id[0] == 'I')
        this->integers[id] -= 1;
    else
        this->floats[id] -= 1;
}
void server::handle_append(std::string contents)
{
    std::string id = contents.substr(0, contents.find(' '));
    std::string to_append = contents.substr(contents.find(' ') + 1);
    this->strings[id] = this->strings[id] + to_append;
}
std::string server::handle_length(std::string contents)
{
    return "" + this->strings[contents].length();
}
