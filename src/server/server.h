/**
 * The MIT License
 * Copyright (c) 2017-2018 Erik Partridge
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef STREAMLABS_SERVER_H
#define STREAMLABS_SERVER_H

#include <string>
#include <unordered_map>
#include <future>
#include <string>
#include <iostream>


class server {

public:
    /**
     * In order for asynchronous requests to work, we'll need some fashion of creating identifiers, that the client can then come back and request the data using said identifier
     * @param type the type of the item
     * @return an almost certainly unique identifier
     */
    std::string create_identifier(std::string type);

    /**
     * Synchronously store an object
     * @param type the type to store
     * @param bytes the content
     * @param async if it should be performed asynchronously
     * @return the identifier of the item in storage
     */
    std::string handle_store(std::string type, std::string bytes, bool async);

    /**
     * Synchronously retrieve an object
     * @param id the id of the item to retrieve
     * @return the item as a string
     */
    std::string handle_get(std::string id);

    /**
     * Take a raw message, parse it, dispatch to the handler, and return a response to write to the pipe
     * @param message the raw message from the client
     * @return the message to write to the client
     */
    std::string parse_message(std::string message);

    std::string handle_message(std::string method, bool async, std::string contents, std::string type);

    bool has_contents();

private:
    std::unordered_map<std::string, long long> integers;
    std::unordered_map<std::string, std::string> strings;
    std::unordered_map<std::string, long double> floats;
    bool store_value(const std::string& type, std::string bytes, const std::string& identifier);
    /**
     * Increment the value of an int stored around here by 1
     * @param basic_string the ID of the integer to increment
     */
    void handle_increment(const std::string& basic_string);
    void handle_decrement(std::string id);
    void handle_append(std::string contents);
    std::string handle_length(std::string contents);
};

#endif //STREAMLABS_SERVER_H
