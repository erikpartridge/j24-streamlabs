/**
 * The MIT License
 * Copyright (c) 2017-2018 Erik Partridge
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef STREAMLABS_SERVER_DAEMON_H
#define STREAMLABS_SERVER_DAEMON_H

#define BUFFER_SIZE 1024

#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <string>
#include <iostream>
#include "server.h"

#define PIPE_NAME "\\\\.\\pipe\\j24"

namespace server_daemon{

    //This is for all intents, the pipe
    HANDLE pipe_handle;
    //How much data was read on the last loop through
    DWORD data_read;
    //Our read buffer
    char buffer[BUFFER_SIZE];

    /* Continuously listen on the pipe and respond to it as appropriate
     */
    void listen_pipe() {
        server* server = new server();

        //Construct the pipe, this will be the pipe server
        pipe_handle = CreateNamedPipe(TEXT(PIPE_NAME),
                    PIPE_ACCESS_DUPLEX, // Two way pipe please
                    PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT, //Set up our communications methods
                    1, // Single pipe
                    BUFFER_SIZE, //Inbound buffer
                    BUFFER_SIZE, //Outbound buffer
                    0, // default wait
                    NULL //No custom security
        );

        while(true){
            bool result = ConnectNamedPipe(pipe_handle, NULL);
            if (result){
                //Great, someone else connected and wrote something! Let's get the contents
                std::string contents = "";
                //Buffer the input
                while (ReadFile(pipe_handle, buffer, BUFFER_SIZE, &data_read, NULL) != false && data_read != 0){
                    //This will automatically construct a string of the length read in.
                    contents += std::string(buffer, data_read);
                }
                //Let the server class do what it does best and handle it
                std::string res = server->parse_message(contents);
                DWORD amount;
                //Then write the response back to the client
                WriteFile(pipe_handle, res,  res.length() + 1, &amount, NULL);
            } else{
                //Some basic error processing
                std::cout << "Encountered a broken pipe, and unable to continue." << std::endl;
                delete server;
                exit(1);
            }
            DisconnectNamedPipe(pipe_handle);
        }
        delete server;
    }

};

#endif //STREAMLABS_SERVER_DAEMON_H
