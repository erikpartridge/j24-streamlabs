/**
 * The MIT License
 * Copyright (c) 2017-2018 Erik Partridge
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "test.h"
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include <windows.h>
#endif

TEST_CASE("Server properly stores incoming message, and returns ID", "[server]"){
    SECTION("Synchronously"){
        server* s = new server();
        std::string id = s->handle_store("INTEGER", "710", false);
        REQUIRE(s->handle_get(id) == "710");
        delete s;
    }
    SECTION("Asynchronously"){
        server* s = new server();
        std::string id = s->handle_store("INTEGER", "710", false);
        #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        Sleep(2);
        #else
        sleep(2);
        #endif
        REQUIRE(s->handle_get(id) == "710");
        delete s;
    }
    SECTION("Parses message properly"){
        std::string content = "STORE\n\ncontent-type: INTEGER\ncontent-length: 2\n1";
        auto* s = new server();
        s->parse_message(content);
        REQUIRE(s->has_contents());
        delete s;
    }
}

TEST_CASE("Client properly formats the request to the server", "[client]") {
    std::string message = "STORE\nasync\ncontent-type: INTEGER\ncontent-length: 2\n1";
    auto* c = new client();
    auto* chars = new char[2];
    chars[0] = '1';
    chars[1] = '\0';
    std::string result = c->build_message("STORE", chars, "INTEGER", true);
    REQUIRE(message == result);
    delete c;
    delete chars;
}

TEST_CASE("Communication between the two is properly facilitated", "[comms]"){
    auto* c = new client();
    auto* chars = new char[4];
    chars[0] = '1';
    chars[1] = '3';
    chars[2] = '0';
    chars[3] = '\0';
    std::string message = c->build_message("STORE", chars, "INTEGER", false);

    auto* s = new server();
    s->parse_message(message);

    REQUIRE(s->has_contents());
    delete s;
    delete c;
    delete chars;
}

TEST_CASE("Can communicate with server via pipe", "[server]"){
    std::cout << "Spawned thread" << std::endl;
    Sleep(2);
    HANDLE hPipe;
    DWORD dwWritten;

    std::cout << "Beginning connection " <<std::endl;
    hPipe = CreateFile(TEXT(PIPE_NAME),
            GENERIC_READ | GENERIC_WRITE,
            0,
            NULL,
            OPEN_EXISTING,
            0,
            NULL);
    std::cout << "Now attempting to write" << std::endl;
    if (hPipe != INVALID_HANDLE_VALUE)
    {
        WriteFile(hPipe,
                "Hello Pipe\n",
                12,   // = length of string + terminating '\0' !!!
                &dwWritten,
                NULL);

        CloseHandle(hPipe);
    }
    exit(0);
}